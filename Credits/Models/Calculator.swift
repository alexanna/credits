//
//  Calculator.swift
//  Credits
//
//  Created by Hanna Homan on 6/7/20.
//  Copyright © 2020 Lexan. All rights reserved.
//

import Foundation

struct PaymentInfo {
    let date: Date
    let percent: Double
    let sum: Double
}

struct CreditInfo {
    let dayPercentSum: Double
    let allPercentSum: Double
    let allSum: Double
    let date: Date
}

class Calculator {
    var creditSum: Double
    var percent: Double
    var days: Int
    var date: Date
    
    lazy var paymentInfoDays: [PaymentInfo] = {
        var paymentInfoDays: [PaymentInfo] = []
        var percentSum = 0.0
        var sum = creditSum
        var currentDate = date
        for _ in 1...days {
            var dayComponent = DateComponents()
            dayComponent.day = 1
            currentDate = Calendar.current.date(byAdding: dayComponent, to: currentDate) ?? currentDate
            percentSum = creditSum * (percent / 100)
            sum = sum + percentSum
            paymentInfoDays.append(PaymentInfo(date: currentDate, percent: percentSum, sum: sum))
        }
        return paymentInfoDays
    }()
    
    lazy var creditInfo: CreditInfo = {
        var dayPercentSum = creditSum * (percent / 100)
        let allPercentSum = paymentInfoDays.map({ $0.percent }).reduce(0, +)
        var dayComponent = DateComponents()
        dayComponent.day = days
        let finishDate = Calendar.current.date(byAdding: dayComponent, to: date) ?? date
        return CreditInfo(dayPercentSum: dayPercentSum, allPercentSum: allPercentSum, allSum: allPercentSum + creditSum, date: finishDate)
    }()

    init(creditSum: Double, percent: Double, days: Int, date: Date) {
        self.creditSum = creditSum
        self.percent = percent
        self.days = days
        self.date = date
    }
}
