//
//  Loan.swift
//  Credits
//
//  Created by Admin on 14.06.2020.
//  Copyright © 2020 Lexan. All rights reserved.
//

import Foundation

struct Loan: Codable {
    var id: String
    var name: String
    var screen: String
    var summ: String
    var score: String
    var term: String
    var percent: String
    var percentPostfix: String
    var mastercard: String
    var mir: String
    var visa: String
    var qiwi: String
    var yandex: String
    var cash: String
    var description: String

    
    enum CodingKeys: String, CodingKey {
        case id
        case name
        case screen
        case summ
        case score
        case term
        case percent
        case percentPostfix
        case mastercard
        case mir
        case visa
        case qiwi
        case yandex
        case cash
		case description
    }
}

/*
 "id": "37",
"name": "еКапуста",
"screen": "https://kredit-zaim.xyz/loans/е-капуста.png",
"summ": "от 100 до 30 000 руб.",
  "score": "5",
"term": "от 7 до 21 дней",
 "percent": "0",
 "percentPostfix": "% в день",
"mastercard": "1",
"mir": "1",
 "visa": "1",
"qiwi": "1",
"yandex": "1",
"cash": "1",
 */
