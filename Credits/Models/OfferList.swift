//
//  Items.swift
//  Credits
//
//  Created by UjiN on 6/7/20.
//  Copyright © 2020 Lexan. All rights reserved.
//

import Foundation

struct OfferList: Codable {
	let offers: [Offer]
	
	enum CodingKeys: String, CodingKey {
	   case offers = "items"
	 }
}
