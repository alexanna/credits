//
//  Glossary.swift
//  Credits
//
//  Created by UjiN on 6/6/20.
//  Copyright © 2020 Lexan. All rights reserved.
//

import Foundation

struct Glossary: Codable {
	let alphabet: String
	let words: [Word]
}


