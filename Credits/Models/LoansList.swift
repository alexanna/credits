//
//  LoansList.swift
//  Credits
//
//  Created by Admin on 14.06.2020.
//  Copyright © 2020 Lexan. All rights reserved.
//

import Foundation

struct LoansList: Codable {
    let loans: [Loan]
    
    enum CodingKeys: String, CodingKey {
       case loans = "loans"
     }
}
