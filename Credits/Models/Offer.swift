//
//  Items.swift
//  Credits
//
//  Created by UjiN on 6/7/20.
//  Copyright © 2020 Lexan. All rights reserved.
//

import Foundation

struct Offer: Codable {
	var name: String
	var maxsum: Int
	var maxDay: Int
	var procent: Int
	
	enum CodingKeys: String, CodingKey {
		case maxDay = "maxdays"
		case name
		case maxsum
		case procent
	}
}
