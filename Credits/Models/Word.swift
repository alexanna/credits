//
//  Word.swift
//  Credits
//
//  Created by UjiN on 6/6/20.
//  Copyright © 2020 Lexan. All rights reserved.
//

import Foundation

struct Word: Codable {
	let title: String
	let description: String
}

