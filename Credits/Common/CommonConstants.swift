//
//  CommonConstants.swift
//  Credits
//
//  Created by Admin on 06.06.2020.
//  Copyright © 2020 Lexan. All rights reserved.
//

import Foundation

struct  CheckListData: Decodable {
    
    let items: [Question]
}

struct Question: Decodable {
    var question:String
    var id:Int
    let items: [Answer]
    
}

struct Answer: Decodable {
    var answer:String
    var id:Int
    var isTrue: Bool
    
}
//        let product: CheckListData = try! JSONDecoder().decode(CheckListData.self, from: jsonFile!)

//struct CheckListData: Codable {
//  var question:String
//  var id:Int
//
//  enum CodingKeys: String, CodingKey {
//    case question
//    case id
//
//  }
//  func encode(to encoder: Encoder) throws {
//    var container = encoder.container(keyedBy: CodingKeys.self)
//    try container.encode(question, forKey: .question)
//    try container.encode(id, forKey: .id)
//  }
//  init(from decoder: Decoder) throws {
//    let container = try decoder.container(keyedBy: CodingKeys.self)
//    question = try container.decode(String.self, forKey: .question)
//    id = try container.decode(Int.self, forKey: .id)
//  }
//}
////////////



/*
 
 struct  ShopListResponse: Decodable {
 enum CodingKeys: String, CodingKey {
 case items
 }
 let items: [Shop]
 }
 
 struct Shop: Decodable {
 var question:String
 var id:Int
 
 
 enum CodingKeys: String, CodingKey {
 case question
 case id
 }
 
 
 init(from decoder: Decoder) throws {
 let container = try decoder.container(keyedBy: CodingKeys.self)
 
 self.question = try! container.decode(String.self, forKey: .question)
 self.id = try! container.decode(Int.self, forKey: .id)
 }
 }
 */


//struct CheckListData: Codable {
//  var question:String
//  var id:Int
//
//  enum CodingKeys: String, CodingKey {
//    case question
//    case id
//
//  }
//  func encode(to encoder: Encoder) throws {
//    var container = encoder.container(keyedBy: CodingKeys.self)
//    try container.encode(question, forKey: .question)
//    try container.encode(id, forKey: .id)
//  }
//  init(from decoder: Decoder) throws {
//    let container = try decoder.container(keyedBy: CodingKeys.self)
//    question = try container.decode(String.self, forKey: .question)
//    id = try container.decode(Int.self, forKey: .id)
//  }
//}
//
//struct CheckListData {
//    let questionData = [
//        (id: 1, name: "Ведете ли вы личный бюджет?"),
//        (id: 2, name: "Зачем нужно вести личный бюджет?"),
//        (id: 3, name: "Что из перечисленного можно считать финансовой целью?")
//    ]
//
//    let answerData = [
//        (id: 1, text: "Ведете ли вы личный бюджет?"),
//        (id: 2, text: "Зачем нужно вести личный бюджет?"),
//        (id: 3, text: "Что из перечисленного можно считать финансовой целью?")
//    ]
//}
