//
//  AppDelegate.swift
//  Credits
//
//  Created by Hanna Homan on 6/5/20.
//  Copyright © 2020 Lexan. All rights reserved.
//

import UIKit
//import Firebase
import IQKeyboardManagerSwift
//import FBSDKCoreKit
//import YandexMobileMetrica
//import Google

@UIApplicationMain


class AppDelegate: UIResponder, UIApplicationDelegate {
	var window : UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        IQKeyboardManager.shared.enable = true
//        ApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOptions)
        //FB
//        FirebaseApp.configure()
        
        // Initializing the AppMetrica SDK.
//        let configuration = YMMYandexMetricaConfiguration.init(apiKey: "bb2c1fb1-211f-4272-8898-86b1226e6bc2")
//        YMMYandexMetrica.activate(with: configuration!)
        //GoogleAnalytics
//        guard let gai = GAI.sharedInstance() else {
//          assert(false, "Google Analytics not configured correctly")
//        }
//        gai.tracker(withTrackingId: "YOUR_TRACKING_ID")
//        // Optional: automatically report uncaught exceptions.
//        gai.trackUncaughtExceptions = true
//
//        // Optional: set Logger to VERBOSE for debug information.
//        // Remove before app release.
//        gai.logger.logLevel = .verbose;
        
//        guard let _ = (scene as? UIWindowScene) else { return }
        if #available(iOS 13, *){
            print("13")
        }else{
            let navigationBarAppearace = UINavigationBar.appearance()
            navigationBarAppearace.tintColor = .white
            navigationBarAppearace.barTintColor = UIColor(named: "redTest")
            navigationBarAppearace.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white ]
            UINavigationBar.appearance().isTranslucent = false

            UIApplication.shared.statusBarStyle = .lightContent
            
            UITabBar.appearance().tintColor = UIColor(named: "redTest")
            UITabBar.appearance().barTintColor = .white

            let tabBarController = UITabBarController()
            
            let loansStoryboard = UIStoryboard(name: "Loans", bundle: nil)
            let loansViewController = loansStoryboard.instantiateInitialViewController()!
            tabBarController.addChild(loansViewController)
            
            let calculatorStoryboard = UIStoryboard(name: "Calculator", bundle: nil)
            let calculatorViewController = calculatorStoryboard.instantiateInitialViewController()!
            tabBarController.addChild(calculatorViewController)
            
            let checkListStoryboard = UIStoryboard(name: "CheckList", bundle: nil)
            let checkListViewController = checkListStoryboard.instantiateInitialViewController()!
            tabBarController.addChild(checkListViewController)
            
            let glossaryStoryboard = UIStoryboard(name: "Glossary", bundle: nil)
            let glossaryViewController = glossaryStoryboard.instantiateInitialViewController()!
            tabBarController.addChild(glossaryViewController)

			window?.rootViewController = tabBarController
            window?.backgroundColor = .white
        }
        return true
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
//        let appId: String = Settings.appID ?? ""
//        if url.scheme != nil && url.scheme!.hasPrefix("fb\(appId)") && url.host ==  "authorize" {
//            return ApplicationDelegate.shared.application(app, open: url, options: options)
//        }
        return false
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
//        AppEvents.activateApp()
    }

    // MARK: UISceneSession Lifecycle

	@available(iOS 13.0, *)
    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

	@available(iOS 13.0, *)
    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }


}

//extension UIApplication {
//
//    var statusBarView: UIView? {
//        return value(forKey: "statusBar") as? UIView
//    }
//
//}
