//
//  AttributedString.swift
//  Credits
//
//  Created by UjiN on 6/17/20.
//  Copyright © 2020 Lexan. All rights reserved.
//

import Foundation

extension NSAttributedString {
	internal convenience init?(html: String) {
		guard let data = html.data(using: String.Encoding.utf16, allowLossyConversion: false) else {
			return nil
		}
		
		guard let attributedString = try?  NSMutableAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding: String.Encoding.utf8.rawValue], documentAttributes: nil) else {
			return nil
		}
		//	attributedString.addAttribute(.font, value: UIFont.systemFont(ofSize: 17), range: NSRange(0..<attributedString.string.count))
		self.init(attributedString: attributedString)
	}
}
