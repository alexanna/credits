//
//  SceneDelegate.swift
//  Credits
//
//  Created by Hanna Homan on 6/5/20.
//  Copyright © 2020 Lexan. All rights reserved.
//

import UIKit

@available(iOS 13.0, *)
class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?
    
    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        // Use this method to optionally configure and attach the UIWindow `window` to the provided UIWindowScene `scene`.
        // If using a storyboard, the `window` property will automatically be initialized and attached to the scene.
        // This delegate does not imply the connecting scene or session are new (see `application:configurationForConnectingSceneSession` instead).

        guard let _ = (scene as? UIWindowScene) else { return }
		
        let navigationBarAppearace = UINavigationBar.appearance()
		navigationBarAppearace.tintColor = .white
        navigationBarAppearace.barTintColor = UIColor(named: "redTest")
        navigationBarAppearace.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white ]
		UINavigationBar.appearance().isTranslucent = false

        UIApplication.shared.statusBarStyle = .lightContent
		
		UITabBar.appearance().tintColor = UIColor(named: "redTest")
		UITabBar.appearance().barTintColor = .white

		let tabBarController = UITabBarController()
		
        let loansStoryboard = UIStoryboard(name: "Loans", bundle: nil)
        let loansViewController = loansStoryboard.instantiateInitialViewController()!
        tabBarController.addChild(loansViewController)
        
        let calculatorStoryboard = UIStoryboard(name: "Calculator", bundle: nil)
        let calculatorViewController = calculatorStoryboard.instantiateInitialViewController()!
        tabBarController.addChild(calculatorViewController)
        
        let checkListStoryboard = UIStoryboard(name: "CheckList", bundle: nil)
        let checkListViewController = checkListStoryboard.instantiateInitialViewController()!
        tabBarController.addChild(checkListViewController)
		
		let glossaryStoryboard = UIStoryboard(name: "Glossary", bundle: nil)
		let glossaryViewController = glossaryStoryboard.instantiateInitialViewController()!
		tabBarController.addChild(glossaryViewController)
        
        window?.rootViewController = tabBarController
		window?.backgroundColor = .white
 //*/
    }

    func sceneDidDisconnect(_ scene: UIScene) {
        // Called as the scene is being released by the system.
        // This occurs shortly after the scene enters the background, or when its session is discarded.
        // Release any resources associated with this scene that can be re-created the next time the scene connects.
        // The scene may re-connect later, as its session was not neccessarily discarded (see `application:didDiscardSceneSessions` instead).
    }

    func sceneDidBecomeActive(_ scene: UIScene) {
        // Called when the scene has moved from an inactive state to an active state.
        // Use this method to restart any tasks that were paused (or not yet started) when the scene was inactive.
    }

    func sceneWillResignActive(_ scene: UIScene) {
        // Called when the scene will move from an active state to an inactive state.
        // This may occur due to temporary interruptions (ex. an incoming phone call).
    }

    func sceneWillEnterForeground(_ scene: UIScene) {
        // Called as the scene transitions from the background to the foreground.
        // Use this method to undo the changes made on entering the background.
    }

    func sceneDidEnterBackground(_ scene: UIScene) {
        // Called as the scene transitions from the foreground to the background.
        // Use this method to save data, release shared resources, and store enough scene-specific state information
        // to restore the scene back to its current state.
    }


}

