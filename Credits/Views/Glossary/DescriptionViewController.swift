//
//  DescriptionViewController.swift
//  Credits
//
//  Created by UjiN on 6/7/20.
//  Copyright © 2020 Lexan. All rights reserved.
//

import UIKit

class DescriptionViewController: UIViewController {
	@IBOutlet weak var titleLabel: UILabel!
	@IBOutlet weak var descriptionLabel: UILabel!
	
	var word: Word?
	
    override func viewDidLoad() {
        super.viewDidLoad()
		title = word?.title
		titleLabel.text = word?.title
		descriptionLabel.text = word?.description
    }
}
