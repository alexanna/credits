//
//  GlossaryViewController.swift
//  Credits
//
//  Created by UjiN on 6/6/20.
//  Copyright © 2020 Lexan. All rights reserved.
//

import UIKit

class GlossaryViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
	var glossarys: [Glossary] = []
	@IBOutlet weak var tableView: UITableView!
	
	override func viewDidLoad() {
		guard let path = Bundle.main.path(forResource: "Glossary", ofType: "json"),
			let data = try? Data(contentsOf: URL(fileURLWithPath: path)) else { return }
		glossarys = try! JSONDecoder().decode([Glossary].self, from: data)
	}
	
	func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
		return glossarys[section].alphabet
	}
	
	func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
		if let headerView = view as? UITableViewHeaderFooterView {
			headerView.contentView.backgroundColor = UIColor(red: 0.973, green: 0.973, blue: 0.973, alpha: 0.82)
			headerView.textLabel?.textColor = UIColor(red: 0.937, green: 0, blue: 0.008, alpha: 1)
		}
	}
	
	func numberOfSections(in tableView: UITableView) -> Int {
		return glossarys.count
	}
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		let glossary = glossarys[section]
		return glossary.words.count
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: "glossaryCell", for: indexPath)
		let fhuguy = glossarys[indexPath.section].words[indexPath.row].title
		cell.textLabel?.text = fhuguy
		return cell
	}
	
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		tableView.deselectRow(at: indexPath, animated: true)
		let word = glossarys[indexPath.section].words[indexPath.row]
		let controller = storyboard?.instantiateViewController(withIdentifier: "DescriptionViewController") as! DescriptionViewController
		
		controller.word = word
		navigationController?.show(controller, sender: nil)
	}
}
