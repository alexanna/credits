//
//  LoansListCell.swift
//  Credits
//
//  Created by Admin on 14.06.2020.
//  Copyright © 2020 Lexan. All rights reserved.
//

import UIKit
import Cosmos

class LoansListCell: UITableViewCell {
	@IBOutlet weak var cosmos: CosmosView!
	@IBOutlet var rootView : UIView!
    @IBOutlet var titleLoansLabel : UILabel!
    @IBOutlet var imageRootLoansView : UIImageView!
    
    @IBOutlet var amountLabel : UILabel!
    @IBOutlet var rateLabel : UILabel!
    @IBOutlet var timeLabel : UILabel!
    
    @IBOutlet var amountValueLabel : UILabel!
    @IBOutlet var rateValueLabel : UILabel!
    @IBOutlet var timeValueLabel : UILabel!
    
    @IBOutlet var additionalButton : UIButton!
	    
	@IBOutlet weak var mirImageView: UIImageView!
	@IBOutlet weak var cashImageView: UIImageView!
	@IBOutlet weak var masterImageView: UIImageView!
	@IBOutlet weak var cardToCardImageView: UIImageView!
	@IBOutlet weak var qiwiImageView: UIImageView!
	@IBOutlet weak var yandexImageView: UIImageView!
	@IBOutlet weak var visaImageView: UIImageView!

	
	override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
	
	

}
