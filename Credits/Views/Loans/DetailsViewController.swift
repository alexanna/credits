//
//  DetailsViewController.swift
//  Credits
//
//  Created by UjiN on 6/14/20.
//  Copyright © 2020 Lexan. All rights reserved.
//

import UIKit
import Cosmos

class DetailsViewController: UIViewController {
	@IBOutlet weak var getupButton: UIButton!
	@IBOutlet weak var cosmosView: CosmosView!
	@IBOutlet weak var bigImageView: UIImageView!
	@IBOutlet weak var nameLabel: UILabel!
	@IBOutlet weak var infoLabel: UILabel!
	@IBOutlet weak var visaImageView: UIImageView!
	@IBOutlet weak var yandexImageView: UIImageView!
	@IBOutlet weak var qiwiImageView: UIImageView!
	@IBOutlet weak var cardsImageView: UIImageView!
	@IBOutlet weak var masterCardImageView: UIImageView!
	@IBOutlet weak var cashImageView: UIImageView!
	@IBOutlet weak var mirImageView: UIImageView!
	
    var objectLoans: Loan!
	
    override func viewDidLoad() {
        super.viewDidLoad()
		let myString = """
		<html>
			  <head>
				<style type="text/css">
				  body {
					font-size: 17px;
				  }
				</style>
			  </head>
			  <body>
		\(objectLoans.description)
			  </body>
			</html>
		"""
		getupButton.layer.cornerRadius = 34
		cosmosView.settings.updateOnTouch = false
		cosmosView.settings.fillMode = .half
		visaImageView.isHidden = objectLoans.visa != "1"
		masterCardImageView.isHidden = objectLoans.mastercard != "1"
		mirImageView.isHidden = objectLoans.mir != "1"
		cashImageView.isHidden = objectLoans.cash != "1"
		yandexImageView.isHidden = objectLoans.yandex != "1"
		qiwiImageView.isHidden = objectLoans.qiwi != "1"
		cardsImageView.isHidden = true
		infoLabel.attributedText = NSAttributedString(html: myString)
		nameLabel.text = objectLoans.name
		if let rate = Double(objectLoans.score) {
			cosmosView.rating = rate
			cosmosView.text = String(rate)
		}
		if let url = URL(string: objectLoans.screen.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed)!) {
			print("url \(url)")
			bigImageView.af.setImage(withURL: url)
		}
        // Do any additional setup after loading the view.
    }
	
	@IBAction func getUp(_ sender: UIButton) {
		
	}
	

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
