//
//  LoansListViewController.swift
//  Credits
//
//  Created by Admin on 14.06.2020.
//  Copyright © 2020 Lexan. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage
import Cosmos

class LoansListViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    
	@IBOutlet weak var tableView : UITableView!
    var myLoans: LoansList?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height - 77)
        tableView.backgroundColor = #colorLiteral(red: 0.8980392157, green: 0.8980392157, blue: 0.8980392157, alpha: 1)//E5E5E5
        getLoans()
    }
    
    func getLoans() {
        AF.request("https://ioszaimback.com/db.json")
            .responseDecodable(of: LoansList.self) { (response) in
                guard let backLoans = response.value else { return }
                self.myLoans = backLoans
                print(self.myLoans!)
                self.tableView.reloadData()
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return myLoans?.loans.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
	
         let cell = tableView.dequeueReusableCell(withIdentifier: "loansListCell", for: indexPath) as! LoansListCell
        cell.backgroundColor = #colorLiteral(red: 0.8980392157, green: 0.8980392157, blue: 0.8980392157, alpha: 1)//E5E5E5
		cell.cosmos.settings.updateOnTouch = false
		cell.cosmos.settings.fillMode = .half
		if let rate = Double(myLoans!.loans[indexPath.row].score) {
			cell.cosmos.rating = rate
			cell.cosmos.text = String(rate)
			}

        cell.titleLoansLabel.text = myLoans?.loans[indexPath.row].name
        cell.amountValueLabel.text = myLoans?.loans[indexPath.row].summ
        var stringR = String((myLoans?.loans[indexPath.row].percent)!)
        let stringL = String((myLoans?.loans[indexPath.row].percentPostfix)!)
        stringR = "\(stringR) \(stringL)"
        cell.rateValueLabel.text = stringR
//        "\(myLoans?.loans[indexPath.row].percent!)
        
        cell.additionalButton.backgroundColor = .white
        cell.additionalButton.layer.cornerRadius = 30
        cell.additionalButton.layer.borderWidth = 2
        cell.additionalButton.layer.borderColor = #colorLiteral(red: 0.937254902, green: 0, blue: 0.007843137255, alpha: 1)//EF0002
        
       cell.additionalButton.tag = indexPath.row
       cell.additionalButton.addTarget(self, action: #selector(self.selectAdditionalButton(_:)), for: .touchUpInside)
        cell.additionalButton.isUserInteractionEnabled = true
		cell.timeValueLabel.text = myLoans?.loans[indexPath.row].term
		
		cell.visaImageView.isHidden = myLoans?.loans[indexPath.row].visa != "1"
		cell.masterImageView.isHidden = myLoans?.loans[indexPath.row].mastercard != "1"
		cell.mirImageView.isHidden = myLoans?.loans[indexPath.row].mir != "1"
		cell.cashImageView.isHidden = myLoans?.loans[indexPath.row].cash != "1"
		cell.yandexImageView.isHidden = myLoans?.loans[indexPath.row].yandex != "1"
		cell.qiwiImageView.isHidden = myLoans?.loans[indexPath.row].qiwi != "1"
		cell.cardToCardImageView.isHidden = true

        print(myLoans?.loans[indexPath.row].screen)
        if let imageUrlString = myLoans?.loans[indexPath.row].screen,
        let url = URL(string: imageUrlString.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed)!) {
        cell.imageRootLoansView.af.setImage(withURL: url)
        }

//        cell.imageRootLoansView.imageFromUrl("https://robohash.org/123.png")
        cell.rootView.layer.cornerRadius =  15
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 484//549
    }
    
    @objc func selectAdditionalButton(_ sender: UIButton) {

        print(sender.tag)
        let objectLoans = (myLoans?.loans[sender.tag])!
        print(objectLoans)
        
        let storyBoard: UIStoryboard = UIStoryboard(name: "Details", bundle: nil)
        let newViewController = storyBoard.instantiateViewController(withIdentifier: "DetailsViewController") as! DetailsViewController
        newViewController.objectLoans = objectLoans
		self.navigationController?.show(newViewController, sender: true)
    }
    
}

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
     
     let right_border = CALayer()
     let right_padding = CGFloat(15.0)
     right_border.borderColor = UIColor.white.cgColor
     right_border.frame = CGRect(x: cell.frame.size.width - right_padding, y: 0, width: right_padding, height: cell.frame.size.height)
     right_border.borderWidth = right_padding

     let left_border = CALayer()
     let left_padding = CGFloat(15.0)
     left_border.borderColor = UIColor.white.cgColor
     left_border.frame = CGRect(x: 0, y: 0, width: left_padding, height: cell.frame.size.height)
     left_border.borderWidth = left_padding
     
     let top_border = CALayer()
     let top_padding = CGFloat(10.0)
     top_border.borderColor = UIColor.white.cgColor
     top_border.frame = CGRect(x: 0, y: 0, width: cell.frame.size.width, height: top_padding)
     top_border.borderWidth = top_padding
     
     cell.layer.addSublayer(top_border)
     cell.layer.addSublayer(right_border)
     cell.layer.addSublayer(left_border)
    */
