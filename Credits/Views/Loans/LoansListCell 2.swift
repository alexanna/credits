//
//  LoansListCell.swift
//  Credits
//
//  Created by Admin on 14.06.2020.
//  Copyright © 2020 Lexan. All rights reserved.
//

import UIKit

class LoansListCell: UITableViewCell {
    @IBOutlet var rootView : UIView!
    @IBOutlet var titleLoansLabel : UILabel!
    @IBOutlet var skilsLoansLabel : UILabel!
    @IBOutlet var imageRootLoansView : UIImageView!
    
    @IBOutlet var amountLabel : UILabel!
    @IBOutlet var rateLabel : UILabel!
    @IBOutlet var timeLabel : UILabel!
    
    @IBOutlet var amountValueLabel : UILabel!
    @IBOutlet var rateValueLabel : UILabel!
    @IBOutlet var timeValueLabel : UILabel!
    
    @IBOutlet var additionalButton : UIButton!
	
	var openDetails: (() -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
openDetails?()
        // Configure the view for the selected state
    }
	
	

}
