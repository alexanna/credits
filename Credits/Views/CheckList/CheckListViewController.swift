//
//  CheckListViewController.swift
//  Credits
//
//  Created by Admin on 06.06.2020.
//  Copyright © 2020 Lexan. All rights reserved.
//

import UIKit

class CheckListViewController: UIViewController {
    @IBOutlet weak var numberQuestionLabel: UILabel!
    @IBOutlet weak var textQuestionLabel: UILabel!
    @IBOutlet weak var firstAnswerButton: UIButton!
    @IBOutlet weak var secondAnswerButton: UIButton!
    @IBOutlet weak var thirdAnswerButton: UIButton!
    @IBOutlet weak var resutView: UIView!
    @IBOutlet weak var resutImage: UIImageView!
    @IBOutlet weak var resutLabel: UILabel!
    @IBOutlet var labelConstraint : NSLayoutConstraint!
    @IBOutlet var buttonConstraint : NSLayoutConstraint!
    
    var questionVar = 0
    var resultCorrectAnswer = 0
    
    var checkListParsResult: CheckListData!
    override func viewDidLoad() {
        super.viewDidLoad()
//        self.setNeedsStatusBarAppearanceUpdate()
        let jsonFile = readLocalFile(forName: "CheckList")
        checkListParsResult = try! JSONDecoder().decode(CheckListData.self, from: jsonFile!)
        print(checkListParsResult!)
        
        firstAnswerButton = setPropertyButton(button: firstAnswerButton)
        secondAnswerButton = setPropertyButton(button: secondAnswerButton)
        thirdAnswerButton = setPropertyButton(button: thirdAnswerButton)
        
        setDataView( questionId: questionVar, answer: 0)
        resutView.isHidden = true
        
        let height = UIScreen.main.nativeBounds.height
        if height == 1136 {
          labelConstraint.constant = 10
          buttonConstraint.constant = 140
        }else{
            labelConstraint.constant = 48
            buttonConstraint.constant = 170
        }
        
        

    }
    
    override func viewWillDisappear(_ animated: Bool) {
        print("viewWillDisappear")
        questionVar = 0
        resultCorrectAnswer = 0
        setDataView( questionId: questionVar, answer: 0)
        firstAnswerButton.isHidden = false
        secondAnswerButton.isHidden = false
        thirdAnswerButton.isHidden = false
        numberQuestionLabel.isHidden = false
        textQuestionLabel.isHidden = false
        
        resutView.isHidden = true
    }
    override func viewDidAppear(_ animated: Bool) {
        print("viewDidAppear")
    }
    @objc func nextQuestion(sender: UIButton) {
        switch sender {
        case firstAnswerButton:
            if checkListParsResult.items[questionVar].items[0].isTrue == true {
                resultCorrectAnswer = resultCorrectAnswer + 1
            }
            print("firstAnswerButton")
        case secondAnswerButton:
            if checkListParsResult.items[questionVar].items[1].isTrue == true {
                resultCorrectAnswer = resultCorrectAnswer + 1
            }
            print("secondAnswerButton")
        case thirdAnswerButton:
            if checkListParsResult.items[questionVar].items[2].isTrue == true {
                resultCorrectAnswer = resultCorrectAnswer + 1
            }
            print("thirdAnswerButton")
        default:
            print("default")
        }
        if questionVar < 9 {
            questionVar = questionVar + 1
            setDataView(questionId: questionVar, answer: 0)
        }
        else{
            print("end")
            setResultView()
        }
    }
    func setResultView(){
        firstAnswerButton.isHidden = true
        secondAnswerButton.isHidden = true
        thirdAnswerButton.isHidden = true
        numberQuestionLabel.isHidden = true
        textQuestionLabel.isHidden = true
        resutView.isHidden = false
        resutView.backgroundColor = .clear
        resutView.frame = CGRect(x: 0, y: textQuestionLabel.frame.origin.y, width: view.frame.width, height: view.frame.height/2)
        //textQuestionLabel.frame.origin.y
        resutLabel.text = "\(resultCorrectAnswer) из 10"
        if resultCorrectAnswer > 7 {
            resutImage.image = #imageLiteral(resourceName: "success")
        }else{
            resutImage.image = #imageLiteral(resourceName: "fail")
        }
    }
    
    func setDataView(questionId: Int, answer:Int) {
//        firstAnswerButton.backgroundColor = UIColor(named: "lightGray")
        firstAnswerButton.setTitle("  \(checkListParsResult.items[questionId].items[answer].answer)  ", for:.normal)
        secondAnswerButton.setTitle("  \(checkListParsResult.items[questionId].items[answer+1].answer)  ", for:.normal)
        thirdAnswerButton.setTitle("  \(checkListParsResult.items[questionId].items[answer+2].answer)  ", for:.normal)
        
        numberQuestionLabel.text = "Вопрос \(checkListParsResult.items[questionId].id)"
        textQuestionLabel.text = checkListParsResult.items[questionId].question
    }
    
    func setPropertyButton(button:UIButton) -> UIButton {
        
        button.titleLabel?.numberOfLines = 0
        button.titleLabel?.lineBreakMode = .byWordWrapping
        button.titleLabel?.textAlignment = .center
        button.sizeToFit()
        button.layer.cornerRadius = 20
        button.addTarget(self, action: #selector(nextQuestion), for: .touchUpInside)//touchDown
        
        button.addTarget(self, action: #selector(buttonDownGray), for: .touchUpInside)//touchDown
        button.addTarget(self, action: #selector(buttonHoldGreen), for: .touchDown)//touchUpInside//green
        
        return button
    }
    
    
    private func readLocalFile(forName name: String) -> Data? {
        do {
            if let bundlePath = Bundle.main.path(forResource: name,
                                                 ofType: "json"),
                let jsonData = try String(contentsOfFile: bundlePath).data(using: .utf8) {
                return jsonData
            }
        } catch {
            print(error)
        }
        
        return nil
    }
    
    //target functions
    @objc func buttonDownGray()
    {
        firstAnswerButton.backgroundColor = #colorLiteral(red: 0.9607843137, green: 0.9529411765, blue: 0.9568627451, alpha: 1) //F5F3F4
        secondAnswerButton.backgroundColor =  #colorLiteral(red: 0.9607843137, green: 0.9529411765, blue: 0.9568627451, alpha: 1) //F5F3F4
        thirdAnswerButton.backgroundColor =  #colorLiteral(red: 0.9607843137, green: 0.9529411765, blue: 0.9568627451, alpha: 1) //F5F3F4
    }
    
    @objc func buttonHoldGreen(sender: UIButton)
    {
        switch sender {
        case firstAnswerButton:
            if checkListParsResult.items[questionVar].items[0].isTrue == true {
                firstAnswerButton.backgroundColor = #colorLiteral(red: 0.2039215686, green: 0.7803921569, blue: 0.3490196078, alpha: 1) //34C759
            }else{
                firstAnswerButton.backgroundColor = #colorLiteral(red: 0.9294117647, green: 0.2509803922, blue: 0.03921568627, alpha: 1)
            }
        case secondAnswerButton:
            if checkListParsResult.items[questionVar].items[1].isTrue == true {
                secondAnswerButton.backgroundColor = #colorLiteral(red: 0.2039215686, green: 0.7803921569, blue: 0.3490196078, alpha: 1)
            }else{
                secondAnswerButton.backgroundColor = #colorLiteral(red: 0.9294117647, green: 0.2509803922, blue: 0.03921568627, alpha: 1) //ED400A
            }
        case thirdAnswerButton:
            if checkListParsResult.items[questionVar].items[2].isTrue == true {
                thirdAnswerButton.backgroundColor = #colorLiteral(red: 0.2039215686, green: 0.7803921569, blue: 0.3490196078, alpha: 1)
            }else{
                thirdAnswerButton.backgroundColor = #colorLiteral(red: 0.9294117647, green: 0.2509803922, blue: 0.03921568627, alpha: 1)
            }
        default:
            print("default")
        }
        
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

}
