//
//  CalculatorViewController.swift
//  Credits
//
//  Created by Hanna Homan on 6/5/20.
//  Copyright © 2020 Lexan. All rights reserved.
//

import UIKit

class CalculatorViewController: UIViewController {
	@IBOutlet weak var zaemChevron: UIImageView!
	@IBOutlet weak var summaLabel: UILabel!
	@IBOutlet weak var summaTextField: UITextField!
	@IBOutlet weak var percentLabel: UILabel!
	@IBOutlet weak var percentTextField: UITextField!
	@IBOutlet weak var offerButton: UIButton!
	@IBOutlet weak var daysLabel: UILabel!
	@IBOutlet weak var daysTextField: UITextField!
	@IBOutlet weak var dateTextField: UITextField!
	@IBOutlet weak var calculateButton: UIButton!
	@IBOutlet weak var summaBackView: UIView!
	@IBOutlet weak var percentBackView: UIView!
	@IBOutlet weak var daysBackView: UIView!
	@IBOutlet weak var dateBackView: UIView!
	@IBOutlet weak var topButtonConstr: NSLayoutConstraint!
	@IBOutlet weak var buttonHeightConstr: NSLayoutConstraint!
	@IBOutlet weak var stack: UIStackView!
	
	var offer: Offer?
	
	override func viewDidLoad() {
		super.viewDidLoad()
		setupUI()
		
		
	}
	
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		if segue.identifier == "CalculateSegue",
			let destination = segue.destination as? CalcResultViewController,
			let offer = offer,
			let date = DateFormatter.standart.date(from: dateTextField.text ?? ""),
			let sum = Double(summaTextField.text ?? ""),
			let days = Int(daysTextField.text ?? "") {
			destination.calculator = Calculator(creditSum: sum, percent: Double(offer.procent), days: days, date: date)
		}
	}
	
	func setupUI() {
		let picker = UIDatePicker()
		picker.datePickerMode = .date
		picker.date = Date()
		picker.minimumDate = Date()
		picker.addTarget(self, action: #selector(dateSelected), for: .valueChanged)
		dateTextField.inputView = picker
		calculateButton.layer.cornerRadius = 34
		daysTextField.keyboardType = .numberPad
		summaTextField.keyboardType = .numberPad
		summaBackView.layer.cornerRadius = 8
		percentBackView.layer.cornerRadius = 8
		daysBackView.layer.cornerRadius = 8
		dateBackView.layer.cornerRadius = 8
		summaLabel.isHidden = true
		percentLabel.isHidden = true
		daysLabel.isHidden = true
		summaTextField.attributedPlaceholder = NSAttributedString(string: "Сумма займа", attributes: [NSAttributedString.Key.foregroundColor: UIColor(named: "grayText") as Any])
		percentTextField.attributedPlaceholder = NSAttributedString(string: "Ставка в день, %", attributes: [NSAttributedString.Key.foregroundColor: UIColor(named: "grayText") as Any])
		daysTextField.attributedPlaceholder = NSAttributedString(string: "Срок займа, дней", attributes: [NSAttributedString.Key.foregroundColor: UIColor(named: "grayText") as Any])
		dateTextField.attributedPlaceholder = NSAttributedString(string: "Дата выдачи", attributes: [NSAttributedString.Key.foregroundColor: UIColor(named: "grayText") as Any])
		percentTextField.isUserInteractionEnabled = false
		offerButton.layer.cornerRadius = 6
		let height = UIScreen.main.nativeBounds.height
		if height == 1136 {
			topButtonConstr.constant = 25
			buttonHeightConstr.constant = 55
			calculateButton.layer.cornerRadius = 26
			stack.spacing = 40
		}
	}
	
	@IBAction func chooseOffer(_ sender: UIButton) {
		let controller = storyboard?.instantiateViewController(withIdentifier: "OfferListViewController") as! OfferListViewController
		controller.offerChoosed = { offer in
			self.offer = offer
			self.updateUI()
		}
		navigationController?.show(controller, sender: nil)
	}
	
	@objc func dateSelected(sender: UIDatePicker) {
		dateTextField.text = DateFormatter.standart.string(from: sender.date)
	}
	
	
	func updateUI() {
		guard let offer = offer else { return }
		summaTextField.isEnabled = true
		percentTextField.isEnabled = true
		daysTextField.isEnabled = true
		dateTextField.isEnabled = true
		calculateButton.isEnabled = true
		dateTextField.text = DateFormatter.standart.string(from: Date())
		daysTextField.text = "\(10)"
		percentTextField.text = "\(offer.procent)"
		summaTextField.text = "\(offer.maxsum)"
		summaLabel.isHidden = false
		percentLabel.isHidden = false
		daysLabel.isHidden = false
		
		let formatter = NumberFormatter()
		formatter.groupingSeparator = " "
		formatter.numberStyle = .decimal
		
		let title = "\(offer.name), до \(formatter.string(from: NSNumber(value: offer.maxsum)) ?? "\(offer.maxsum)"), \(offer.procent)% в день"
		offerButton.setTitle(title, for: .normal)
	}
	
	@IBAction func calculate(_ sender: Any) {
		guard offer != nil && !(dateTextField.text?.isEmpty ?? true) && !(summaTextField.text?.isEmpty ?? true) && !(daysTextField.text?.isEmpty ?? true) else {
			let alert = UIAlertController(title: "Внимание", message: "Заполните все поля", preferredStyle: .alert)
			alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
			present(alert, animated: true, completion: nil)
			return
		}
		performSegue(withIdentifier: "CalculateSegue", sender: nil)
	}
	
}

extension CalculatorViewController: UITextFieldDelegate {
	func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
		let textFieldText: NSString = (textField.text ?? "") as NSString
		let txtAfterUpdate = textFieldText.replacingCharacters(in: range, with: string)
		
		if let number = Int(txtAfterUpdate), let offer = offer {
			if textField == daysTextField && number > offer.maxDay {
				return false
			}
			if textField == summaTextField && number > offer.maxsum {
				return false
			}
		}
		return true
	}
	
	func textFieldDidBeginEditing(_ textField: UITextField) {
		if textField == summaTextField {
			summaLabel.textColor = UIColor(named: "redTest")
			summaBackView.layer.borderWidth = 1
			summaBackView.layer.borderColor = UIColor(named: "redTest")?.cgColor
		} else if textField == daysTextField {
			daysLabel.textColor = UIColor(named: "redTest")
			daysBackView.layer.borderWidth = 1
			daysBackView.layer.borderColor = UIColor(named: "redTest")?.cgColor
		} else if textField == dateTextField {
			dateBackView.layer.borderWidth = 1
			dateBackView.layer.borderColor = UIColor(named: "redTest")?.cgColor
		}
	}
	
	func textFieldDidEndEditing(_ textField: UITextField) {
		summaBackView.layer.borderWidth = 0
		daysBackView.layer.borderWidth = 0
		dateBackView.layer.borderWidth = 0
		if textField == summaTextField && textField.text == "" {
			summaLabel.isHidden = true
		} else {
			summaLabel.isHidden = false
			summaLabel.textColor = UIColor(named: "grayText")
		}
		if textField == daysTextField && textField.text == "" {
			daysLabel.isHidden = true
		} else {
			daysLabel.isHidden = false
			daysLabel.textColor = UIColor(named: "grayText")
		}
	}
}
