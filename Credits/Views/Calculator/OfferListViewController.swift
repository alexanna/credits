//
//  OfferListViewController.swift
//  Credits
//
//  Created by Hanna Homan on 6/6/20.
//  Copyright © 2020 Lexan. All rights reserved.
//

import UIKit
import Alamofire

class OfferListViewController: UITableViewController {
	
	var myOffers: OfferList?
	var offerChoosed: ((Offer) -> Void)?
	
	override func viewDidLoad() {
		getOffer()
	}
	
	func getOffer() {
		AF.request("https://ioszaimback.com/calc.json")
			.responseDecodable(of: OfferList.self) { (response) in
				guard let backOffers = response.value else { return }
				self.myOffers = backOffers
				self.tableView.reloadData()
		}
	}
	
	override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return myOffers?.offers.count ?? 0
	}
	
	override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: "offerCell", for: indexPath)
		let formatter = NumberFormatter()
        formatter.groupingSeparator = " "
        formatter.numberStyle = .decimal
		if let offer = myOffers?.offers[indexPath.row] {
			cell.textLabel?.text = "\(offer.name), до \(formatter.string(from: NSNumber(value: offer.maxsum)) ?? "\(offer.maxsum)"), \(offer.procent)% в день"
		}
		return cell
	}
	
	override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		tableView.deselectRow(at: indexPath, animated: true)
		if let offer = myOffers?.offers[indexPath.row] {
			offerChoosed?(offer)
			navigationController?.popViewController(animated: true)
		}
	}
}
