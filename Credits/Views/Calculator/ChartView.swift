//
//  ChartView.swift
//  Credits
//
//  Created by Hanna Homan on 6/6/20.
//  Copyright © 2020 Lexan. All rights reserved.
//

import UIKit

class ChartView: UIView {
    var min: CGFloat?
    var max: CGFloat?
        
    func update(with min: Double, max: Double) {
        self.min = CGFloat(min)
        self.max = CGFloat(max)
        setupUI()
    }
    
    private func setupUI() {
        guard let min = min, let max = max else { return }
        
        subviews.forEach({ $0.removeFromSuperview() })
        let validWidth = frame.width - 50
        let maxWidth = validWidth * 0.75
        let minWidth = (maxWidth * min) / max
        
        let full = (max / maxWidth) * validWidth
        var step: Int = Int((full) / 5)
        if step > 1000 {
            step = Int(step / 1000) * 1000
        } else if step > 100 {
            step = Int(step / 100) * 100
        }
        
        let stepWidth = CGFloat(step) * maxWidth / max
        let lineColor = UIColor(red: 0.8, green: 0.8, blue: 0.8, alpha: 1)
        
        let topSeparator = UIView()
        topSeparator.backgroundColor = lineColor
        addSubview(topSeparator)
        topSeparator.heightAnchor.constraint(equalToConstant: 1).isActive = true
        topSeparator.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
        topSeparator.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
        topSeparator.topAnchor.constraint(equalTo: topAnchor).isActive = true
        topSeparator.translatesAutoresizingMaskIntoConstraints = false

        let bottomSeparator = UIView()
        bottomSeparator.backgroundColor = lineColor
        addSubview(bottomSeparator)
        bottomSeparator.heightAnchor.constraint(equalToConstant: 1).isActive = true
        bottomSeparator.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
        bottomSeparator.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
        bottomSeparator.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -15).isActive = true
        bottomSeparator.translatesAutoresizingMaskIntoConstraints = false

        let line1 = UIView()
        line1.backgroundColor = lineColor
        addSubview(line1)
        line1.leadingAnchor.constraint(equalTo: leadingAnchor, constant: stepWidth).isActive = true
        line1.widthAnchor.constraint(equalToConstant: 1).isActive = true
        line1.topAnchor.constraint(equalTo: topAnchor).isActive = true
        line1.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -15).isActive = true
        line1.translatesAutoresizingMaskIntoConstraints = false

        let line2 = UIView()
        line2.backgroundColor = lineColor
        addSubview(line2)
        line2.widthAnchor.constraint(equalToConstant: 1).isActive = true
        line2.leadingAnchor.constraint(equalTo: line1.leadingAnchor, constant: stepWidth).isActive = true
        line2.topAnchor.constraint(equalTo: topAnchor).isActive = true
        line2.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -15).isActive = true
        line2.translatesAutoresizingMaskIntoConstraints = false

        let line3 = UIView()
        line3.backgroundColor = lineColor
        addSubview(line3)
        line3.widthAnchor.constraint(equalToConstant: 1).isActive = true
        line3.leadingAnchor.constraint(equalTo: line2.leadingAnchor, constant: stepWidth).isActive = true
        line3.topAnchor.constraint(equalTo: topAnchor).isActive = true
        line3.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -15).isActive = true
        line3.translatesAutoresizingMaskIntoConstraints = false

        let line4 = UIView()
        line4.backgroundColor = lineColor
        addSubview(line4)
        line4.widthAnchor.constraint(equalToConstant: 1).isActive = true
        line4.leadingAnchor.constraint(equalTo: line3.leadingAnchor, constant: stepWidth).isActive = true
        line4.topAnchor.constraint(equalTo: topAnchor).isActive = true
        line4.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -15).isActive = true
        line4.translatesAutoresizingMaskIntoConstraints = false

        let line5 = UIView()
        line5.backgroundColor = lineColor
        addSubview(line5)
        line5.widthAnchor.constraint(equalToConstant: 1).isActive = true
        line5.leadingAnchor.constraint(equalTo: line4.leadingAnchor, constant: stepWidth).isActive = true
        line5.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -15).isActive = true
        line5.topAnchor.constraint(equalTo: topAnchor).isActive = true
        line5.translatesAutoresizingMaskIntoConstraints = false
        
        let formatter = NumberFormatter()
        formatter.groupingSeparator = " "
        formatter.decimalSeparator = ","
        formatter.numberStyle = .decimal

        let label1 = UILabel()
        label1.textColor = lineColor
        label1.font = .systemFont(ofSize: 12)
        label1.text = formatter.string(from: NSNumber(value: step))
        label1.translatesAutoresizingMaskIntoConstraints = false
        addSubview(label1)
        label1.centerXAnchor.constraint(equalTo: line1.centerXAnchor).isActive = true
        label1.topAnchor.constraint(equalTo: line1.bottomAnchor, constant: 4).isActive = true

        let label2 = UILabel()
        label2.textColor = lineColor
        label2.font = .systemFont(ofSize: 12)
        label2.text = formatter.string(from: NSNumber(value: step * 2))
        label2.translatesAutoresizingMaskIntoConstraints = false
        addSubview(label2)
        label2.centerXAnchor.constraint(equalTo: line2.centerXAnchor).isActive = true
        label2.topAnchor.constraint(equalTo: line2.bottomAnchor, constant: 4).isActive = true

        let label3 = UILabel()
        label3.textColor = lineColor
        label3.font = .systemFont(ofSize: 12)
        label3.text = formatter.string(from: NSNumber(value: step * 3))
        label3.translatesAutoresizingMaskIntoConstraints = false
        addSubview(label3)
        label3.centerXAnchor.constraint(equalTo: line3.centerXAnchor).isActive = true
        label3.topAnchor.constraint(equalTo: line3.bottomAnchor, constant: 4).isActive = true

        let label4 = UILabel()
        label4.textColor = lineColor
        label4.font = .systemFont(ofSize: 12)
        label4.text = formatter.string(from: NSNumber(value: step * 4))
        addSubview(label4)
        label4.translatesAutoresizingMaskIntoConstraints = false
        label4.centerXAnchor.constraint(equalTo: line4.centerXAnchor).isActive = true
        label4.topAnchor.constraint(equalTo: line4.bottomAnchor, constant: 4).isActive = true

        let label5 = UILabel()
        label5.textColor = lineColor
        label5.font = .systemFont(ofSize: 12)
        label5.text = formatter.string(from: NSNumber(value: step * 5))
        addSubview(label5)
        label5.translatesAutoresizingMaskIntoConstraints = false
        label5.centerXAnchor.constraint(equalTo: line5.centerXAnchor).isActive = true
        label5.topAnchor.constraint(equalTo: line5.bottomAnchor, constant: 4).isActive = true

        let minView = UIView()
        minView.backgroundColor = UIColor(red: 0.8, green: 0.8, blue: 0.8, alpha: 1)
        addSubview(minView)
        minView.heightAnchor.constraint(equalToConstant: 69).isActive = true
        minView.topAnchor.constraint(equalTo: topAnchor, constant: 15).isActive = true
        minView.widthAnchor.constraint(equalToConstant: minWidth).isActive = true
        minView.translatesAutoresizingMaskIntoConstraints = false

        let maxView = UIView()
        maxView.backgroundColor = UIColor(red: 0.204, green: 0.78, blue: 0.349, alpha: 1)
        addSubview(maxView)
        maxView.heightAnchor.constraint(equalToConstant: 69).isActive = true
        maxView.topAnchor.constraint(equalTo: minView.bottomAnchor, constant: 15).isActive = true
        maxView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -30).isActive = true
        maxView.widthAnchor.constraint(equalToConstant: maxWidth).isActive = true
        maxView.translatesAutoresizingMaskIntoConstraints = false
        
        formatter.maximumFractionDigits = 2
        formatter.minimumFractionDigits = 2

        let minLabel = UILabel()
        minLabel.textColor = .white
        if let minText = formatter.string(from: NSNumber(value: Int(min))) {
            minLabel.text = "Берете: \(minText) Р"
        }
        minLabel.font = .systemFont(ofSize: 17)
        addSubview(minLabel)
        minLabel.translatesAutoresizingMaskIntoConstraints = false
        minLabel.leadingAnchor.constraint(greaterThanOrEqualTo: minView.leadingAnchor, constant: 4).isActive = true
        minLabel.trailingAnchor.constraint(equalTo: minView.trailingAnchor, constant: -16).isActive = true
        minLabel.centerYAnchor.constraint(equalTo: minView.centerYAnchor).isActive = true
        minLabel.adjustsFontSizeToFitWidth = true
        
        let maxLabel = UILabel()
        maxLabel.textColor = .white
        if let maxText = formatter.string(from: NSNumber(value: Int(max))) {
            maxLabel.text = "Вы вернете: \(maxText) Р"
        } 
        maxLabel.font = .systemFont(ofSize: 17)
        addSubview(maxLabel)
        maxLabel.translatesAutoresizingMaskIntoConstraints = false
        maxLabel.trailingAnchor.constraint(equalTo: maxView.trailingAnchor, constant: -16).isActive = true
        maxLabel.centerYAnchor.constraint(equalTo: maxView.centerYAnchor).isActive = true
        
        layoutIfNeeded()
    }
}
