//
//  CalcResultViewController.swift
//  Credits
//
//  Created by Hanna Homan on 6/6/20.
//  Copyright © 2020 Lexan. All rights reserved.
//

import UIKit

class CalcResultViewController: UITableViewController {
    @IBOutlet weak var chartView: ChartView!
    @IBOutlet weak var daySumLabel: UILabel!
    @IBOutlet weak var overpayLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var paySumLabel: UILabel!
    @IBOutlet weak var submitButton: UIButton!
    var calculator: Calculator!
    
    override func viewDidLoad() {
        chartView.update(with: calculator.creditSum, max: calculator.creditInfo.allSum)
        submitButton.layer.cornerRadius = 34
        daySumLabel.text = "\(NumberFormatter.standart.string(from: NSNumber(value: calculator.creditInfo.dayPercentSum)) ?? "") P"
        overpayLabel.text = "\(NumberFormatter.standart.string(from: NSNumber(value: calculator.creditInfo.allPercentSum)) ?? "") P"
        dateLabel.text = DateFormatter.standart.string(from: calculator.creditInfo.date)
        paySumLabel.text = "\(NumberFormatter.standart.string(from: NSNumber(value: calculator.creditInfo.allSum)) ?? "") P"
    }
    
    @IBAction func submitApplication(_ sender: Any) {
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return calculator.paymentInfoDays.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CalculatorTableViewCell", for: indexPath) as! CalculatorTableViewCell
        let info = calculator.paymentInfoDays[indexPath.row]
        cell.dateLabel.text = DateFormatter.standart.string(from: info.date)
        cell.payLabel.text = "\(NumberFormatter.standart.string(from: NSNumber(value: info.sum)) ?? "") P"
        cell.percentLabel.text = "\(NumberFormatter.standart.string(from: NSNumber(value: info.percent)) ?? "") P"
        return cell
    }
}
