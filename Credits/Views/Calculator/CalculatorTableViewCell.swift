//
//  CalculatorTableViewCell.swift
//  Credits
//
//  Created by Hanna Homan on 6/7/20.
//  Copyright © 2020 Lexan. All rights reserved.
//

import UIKit

class CalculatorTableViewCell: UITableViewCell {
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var percentLabel: UILabel!
    @IBOutlet weak var payLabel: UILabel!
    
}
