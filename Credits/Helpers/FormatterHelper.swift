//
//  FormatterHelper.swift
//  Credits
//
//  Created by Hanna Homan on 6/7/20.
//  Copyright © 2020 Lexan. All rights reserved.
//

import Foundation

extension NumberFormatter {
    static var standart: NumberFormatter {
        let formatter = NumberFormatter()
        formatter.groupingSeparator = " "
        formatter.decimalSeparator = ","
        formatter.numberStyle = .decimal
        formatter.maximumFractionDigits = 2
        formatter.minimumFractionDigits = 2
        return formatter
    }
}

extension DateFormatter {
    static var standart: DateFormatter {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd.MM.yyyy"
        return formatter
    }
}
